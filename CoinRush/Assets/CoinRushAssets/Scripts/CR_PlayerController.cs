﻿using UnityEngine;
using System.Collections;

public class CR_PlayerController : MonoBehaviour {

	string direction;
	Animator anim;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}


	protected virtual void PlayerMovement()
	{
		//left movement
		if (Input.GetKey ("a") || Input.GetKey ("left")) {
				SetDirection ('l');
				gameObject.transform.position = new Vector3 (transform.position.x /*- speed*/, transform.position.y, transform.position.z);
				//right movement
		} else if (Input.GetKey ("d") || Input.GetKey ("right")) {
				SetDirection ('r');
			gameObject.transform.position = new Vector3 (transform.position.x /*+ speed*/, transform.position.y, transform.position.z);
		} else if (Input.GetKey ("w") || Input.GetKey ("up")) {  //up movement
				SetDirection ('u');
			gameObject.transform.position = new Vector3 (transform.position.x, transform.position.y /*+ speed*/, transform.position.z);
		} else if (Input.GetKey ("s") || Input.GetKey ("down") /*&& (!hitStop)*/) {  //down movement
				SetDirection ('d');
				gameObject.transform.position = new Vector3 (transform.position.x, transform.position.y /*- speed*/, transform.position.z);
		}
	}

		public virtual string SetDirection(char directionCode)
		{
			//entity facing West
			if (directionCode.Equals('l'))
			{
				direction = "W";
				//animScript.SetSpriteDirection(4);
			}//entity facing Northwest
			else if (directionCode.Equals('w'))
			{
				if (direction.Equals("N") || direction.Equals("W") || direction.Equals("NW"))
				{
				}else //animScript.SetSpriteDirection(8);
				
				direction = "NW";
			}//entity facing North
			else if (directionCode.Equals('u'))
			{
				direction = "N";
				//animScript.SetSpriteDirection(8);
			}//entity facing Northeast
			else if (directionCode.Equals('x'))
			{
				if (direction.Equals("N") || direction.Equals("E") || direction.Equals("NE"))
				{
				}
				else //animScript.SetSpriteDirection(8);
				direction = "NE";
			}//entity facing East
			else if (directionCode.Equals('r'))
			{
				direction = "E";
				//animScript.SetSpriteDirection(6);
			}//entity facing Southeast
			else if (directionCode.Equals('y'))
			{
				if (direction.Equals("S") || direction.Equals("E") || direction.Equals("SE"))
				{
				}
				else //animScript.SetSpriteDirection(2);
				direction = "SE";
			}//entity facing South
			else if (directionCode.Equals('d'))
			{
				direction = "S";
				//animScript.SetSpriteDirection(2);
			}//entity facing Southwest
			else if (directionCode.Equals('z'))
			{
				if (direction.Equals("S") || direction.Equals("W") || direction.Equals("SW"))
				{
				}
				else //animScript.SetSpriteDirection(2);
				direction = "SW";
			}
			return direction;
		}

}
