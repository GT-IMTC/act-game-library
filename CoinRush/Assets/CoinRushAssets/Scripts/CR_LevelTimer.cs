﻿using UnityEngine;
using System.Collections;

public class CR_LevelTimer : MonoBehaviour {

    public float totalTimeLeft;
    private float displayedTime;
    private GUIText TimerText;

	// Use this for initialization
	void Start () {
        TimerText = gameObject.GetComponent<GUIText>();
	}
	
	// Update is called once per frame
	void Update () {
        if (totalTimeLeft > 0)
        {
            totalTimeLeft -= Time.deltaTime;
            displayedTime = Mathf.Floor(totalTimeLeft * 100f) / 100f;
            TimerText.text = displayedTime.ToString("00.00");
        }
        else
        {
            totalTimeLeft = 0;
            TimerText.text = "" + totalTimeLeft;
        }
	}
}
