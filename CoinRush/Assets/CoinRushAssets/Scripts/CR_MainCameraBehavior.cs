﻿using UnityEngine;
using System.Collections;

public class CR_MainCameraBehavior : MonoBehaviour {

    public ThirdPersonCharacter player;

	// Use this for initialization
	void Start () {
        player = FindObjectOfType<ThirdPersonCharacter>();
	}
	
	// Update is called once per frame
	void Update () {
        transform.position = new Vector3(player.gameObject.transform.position.x, player.gameObject.transform.position.y+4f, transform.position.z);
	}
}
